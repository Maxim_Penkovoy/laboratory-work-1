# LABORATORY WORK No. 1

## General task:
It is necessary to create a program that will perform the functions of a notebook
phone book. The program should provide the ability to:

1. Create a new record, including the ability to specify the following information for each of the records:
    1. Surname;
	2. Name;
	3. Patronymic (this field is optional);
	4. Phone number (numbers only);
	5. Country;
	6. Date of birth (this field is optional);
	7. Organization (this field is optional);
	8. Position (this field is optional);
	9. Other notes (this field is optional).
2. Editing the created records.
3. Deleting created records.
4. Viewing the created accounts.
5. View all created accounts, with brief information, including the following main fields:
	1. Surname;
	2. Name;
	3. Phone number.
	
The program should be a classic console application,
implemented using the .Net technology stack, provide an interactive
interface to interact with the user and be able to session
storing the created information.