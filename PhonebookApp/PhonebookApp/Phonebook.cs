﻿using System;
using System.Collections.Generic;

namespace PhonebookApp
{
    class Phonebook
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to \"The phone book\".");
            PrintListOfCommands();
            List<Person> personList = new List<Person>();
            //personList.Add(new Person()
            //{
            //    Surname = "penkovoy",
            //    Name = "maxim",
            //    Patronymic = "aleksandrovich",
            //    Number = "111",
            //    Country = "russia",
            //    Birthday = Convert.ToDateTime("14/04/1998"),
            //    Organization = "itmo",
            //    Position = "student",
            //    Notes = "alalaalalalalalala"
            //});
            //personList.Add(new Person()
            //{
            //    Surname = "penkovoy",
            //    Name = "maxim",
            //    Patronymic = "aleksandrovich",
            //    Number = "111",
            //    Country = "russia",
            //    Birthday = Convert.ToDateTime("14/04/1998"),
            //    Organization = "spbgu",
            //    Position = "teacher",
            //    Notes = "olololololololo"
            //});
            //personList.Add(new Person()
            //{
            //    Surname = "ivanov",
            //    Name = "ivan",
            //    Patronymic = "ivanovich",
            //    Number = "222",
            //    Country = "belarus",
            //    Birthday = Convert.ToDateTime("11/11/1992"),
            //    Organization = "spbgu",
            //    Position = "teacher",
            //    Notes = "trolololololololo"
            //});

            while (true)
            {
                Console.Write(">");
                string command = Console.ReadLine().Trim();
                if (!(command == "exit"))
                {
                    switch (command)
                    {
                        case "create":
                            Create(personList);
                            break;
                        case "edit":
                            Edit(personList);
                            break;
                        case "delete":
                            Delete(personList);
                            break;
                        case "view":
                            View(personList);
                            break;
                        case "viewsum":
                            ViewSum(personList);
                            break;
                        case "help":
                            PrintListOfCommands();
                            break;
                        case "":
                            break;
                        default:
                            Console.WriteLine($"\"{command}\" is not command. Use \"help\" to display a list of available commands.\n");
                            break;
                    }
                }
                else
                {
                    break;
                }
            }
        }

        public static void PrintListOfCommands()
        {
            Console.WriteLine("List of available commands:\n" +
                "   create - creat a note,\n" +
                "   edit - edit the selected note,\n" +
                "   delete - delete the selected note,\n" +
                "   view - view full information about the selected note,\n" +
                "   viewsum - view summary information for all notes,\n" +
                "   help - display a list of available commands,\n" +
                "   exit - exit the program.\n");
        }

        public static void ViewSum(List<Person> personList)
        {
            if (personList.Count > 0)
            {
                for (int i = 0; i < personList.Count; i++)
                {
                    Console.WriteLine(personList[i].Surname + " " + personList[i].Name + " " + personList[i].Number);
                }
            }
            else
            {
                Console.WriteLine("The phone book is empty.");
            }
            Console.WriteLine();
        }

        //selects an element from List<Person> personList with the required surname field value, 
        //if List<Person> personList contains more than one such elements, it displays them in a numbered list 
        //in the format of the command ViewSum and asks to select the desired one and select it.
        public static Person SelectPerson(List<Person> personList)
        {
            int indexOfSelectedPerson = 0;
            int sumSelectedPerson = 0;
            bool isCorrectSelection = false;
            List<Person> selectedPerson = new List<Person>();
            Console.Write("Select the surname of the person: ");
            string surname = Console.ReadLine();
            for (int i = 0; i < personList.Count; i++)
            {
                if (personList[i].Surname == surname)
                {
                    indexOfSelectedPerson = i;
                    sumSelectedPerson++;
                    selectedPerson.Add(personList[i]);
                }
            }
            if (sumSelectedPerson == 0)
            {
                Console.WriteLine("You need to enter the surname of an existing note.\n");
            }
            else if (sumSelectedPerson != 1)
            {
                Console.WriteLine("The phone book has several notes with this surname.");
                for (int i = 0; i < selectedPerson.Count; i++)
                {
                    Console.WriteLine(i + 1 + " " + personList[i].Surname + " " + personList[i].Name + " " + personList[i].Number);
                }
                Console.WriteLine();
                Console.Write("Enter index number of notes for clarification: ");
                int number;
                if (!int.TryParse(Console.ReadLine(), out number) || number > selectedPerson.Count || number <= 0)
                {
                    Console.WriteLine("You need to enter a index number from the proposed list.\n");
                }
                else
                {
                    indexOfSelectedPerson = number - 1;
                    isCorrectSelection = true;
                }
            }
            else
            {
                isCorrectSelection = true;
            }
            if (isCorrectSelection)
            {
                return personList[indexOfSelectedPerson];
            }
            else
            {
                return null;
            }
        }

        public static void Create(List<Person> personList)
        {
            Person createdPerson = Person.CreatePerson();
            if (createdPerson != null)
            {
                personList.Add(createdPerson);
            }
        }

        public static void Edit(List<Person> personList)
        {
            Person selectedPerson = SelectPerson(personList);
            if (selectedPerson != null)
            {
                Person.EditPerson(selectedPerson);
            }
        }

        public static void Delete(List<Person> personList)
        {
            Person selectedPerson = SelectPerson(personList);
            if (selectedPerson != null)
            {
                while (true)
                {
                    Console.Write("Are you sure? Y/N: ");
                    string decision = Console.ReadLine();
                    if (decision.ToUpper() == "Y")
                    {
                        personList.Remove(selectedPerson);
                        break;
                    }
                    else if (decision.ToUpper() == "N")
                    {
                        break;
                    }
                    else
                    {
                        Console.Write("Enter \"Y\" or \"N\".\n");
                    }

                }
            }
        }

        public static void View(List<Person> personList)
        {
            Person selectedPerson = SelectPerson(personList);
            if (selectedPerson != null)
            {
                Console.WriteLine(selectedPerson.ToString());
            }
        }
    }
}
