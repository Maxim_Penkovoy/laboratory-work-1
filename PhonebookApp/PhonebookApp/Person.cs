﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhonebookApp
{
    class Person
    {
        public string Surname { get; set; }
        public string Name { get; set; }
        public string Patronymic { get; set; }
        public string Number { get; set; }
        public string Country { get; set; }
        public DateTime Birthday { get; set; }
        public string Organization { get; set; }
        public string Position { get; set; }
        public string Notes { get; set; }

        public override string ToString()
        {
            string fullInfo =
                $"   Surname: {Surname}\n" +
                $"   Name: {Name}\n" +
                $"   LastName: {(string.IsNullOrEmpty(Patronymic) ? "Value not set" : Patronymic)}\n" +
                $"   Number: {Number}\n" +
                $"   Country: {Country}\n" +
                $"   DateOfBirthDay: {(Birthday.Ticks == 0 ? "Value not set" : Birthday.ToString("dd'/'MM'/'yyyy"))}\n" +
                $"   Organization: {(string.IsNullOrEmpty(Organization) ? "Value not set" : Organization)}\n" +
                $"   Position: {(string.IsNullOrEmpty(Position) ? "Value not set" : Position)}\n" +
                $"   Notes: {(string.IsNullOrEmpty(Notes) ? "Value not set" : Notes)}\n";
            return fullInfo;
        }

        public static Person CreatePerson()
        {
            Person person = new Person();
            string tempRead;

            Console.Write("Please enter surname (this field is required): ");
            tempRead = Console.ReadLine();
            if (string.IsNullOrEmpty(tempRead))
            {
                Console.WriteLine("Surname value cannot be empty.\n");
                return null;
            }
            else
            {
                person.Surname = tempRead;
            }

            Console.Write("Please enter name (this field is required): ");
            tempRead = Console.ReadLine();
            if (string.IsNullOrEmpty(tempRead))
            {
                Console.WriteLine("Name value cannot be empty.\n");
                return null;
            }
            else
            {
                person.Name = tempRead;
            }

            Console.Write("Please enter patronymic (this field is optional): ");
            person.Patronymic = Console.ReadLine();

            Console.Write("Please enter phone number (this field is required and must be only digits): ");
            tempRead = Console.ReadLine();
            bool isDigit = true;
            for (int i = 0; i < tempRead.Length; i++)
            {
                isDigit = char.IsDigit(tempRead[i]) && isDigit;
            }
            if (string.IsNullOrEmpty(tempRead) || !isDigit)
            {
                Console.WriteLine("Phone number value cannot be empty and must be only digits.\n");
                return null;
            }
            else
            {
                person.Number = tempRead;
            }

            Console.Write("Please enter country (this field is required): ");
            tempRead = Console.ReadLine();
            if (string.IsNullOrEmpty(tempRead))
            {
                Console.WriteLine("Country value cannot be empty.\n");
                return null;
            }
            else
            {
                person.Country = tempRead;
            }

            Console.Write("Please enter date of birth in the following format dd/MM/yyyy (this field is optional): ");
            tempRead = Console.ReadLine();
            DateTime Birthday;
            if (DateTime.TryParse(tempRead, out Birthday) || string.IsNullOrEmpty(tempRead))
            {
                person.Birthday = Birthday;
            }
            else
            {
                Console.WriteLine("Incorrect value entered for date of birth.\n");
                return null;
            }

            Console.Write("Please enter organization (this field is optional): ");
            person.Organization = Console.ReadLine();

            Console.Write("Please enter position (this field is optional): ");
            person.Position = Console.ReadLine();

            Console.Write("Please enter other notes (this field is optional): ");
            person.Notes = Console.ReadLine();

            return person;
        }

        public static void EditPerson(Person person)
        {
            List<string> listFieldNames = new List<string>() { "surname", "name", "lastname", "number", "country",
                "birthday", "organization", "position", "notes"};
            Console.WriteLine("List of available fields for editing:");
            for (int i = 0; i < listFieldNames.Count; i++)
            {
                Console.WriteLine(listFieldNames[i]);
            }
            Console.WriteLine();

            Console.Write("Select the field you want to edit: ");
            string fieldName = Console.ReadLine();

            if (listFieldNames.Contains(fieldName))
            {
                Console.Write($"Enter the value you want to assign to the field {fieldName} ");
                if (fieldName == "surname" || fieldName == "name" || fieldName == "country")
                {
                    Console.Write("(this field is required): ");
                }
                else if (fieldName == "number")
                {
                    Console.Write("(this field is required and must be only digits): ");
                }
                else if (fieldName == "birthday")
                {
                    Console.Write("(required date format: dd/MM/yyyy): ");
                }

                string newValue = Console.ReadLine();
                switch (fieldName)
                {
                    case "surname":
                        if (string.IsNullOrEmpty(newValue))
                        {
                            Console.WriteLine("Surname field cannot be empty.\n");
                        }
                        else
                        {
                            person.Surname = newValue;
                        }
                        break;
                    case "name":
                        if (string.IsNullOrEmpty(newValue))
                        {
                            Console.WriteLine("Name value cannot be empty.\n");
                        }
                        else
                        {
                            person.Name = newValue;
                        }
                        break;
                    case "lastname":
                        person.Patronymic = newValue;
                        break;
                    case "number":
                        bool isDigit = true;
                        for (int i = 0; i < newValue.Length; i++)
                        {
                            isDigit = char.IsDigit(newValue[i]) && isDigit;
                        }
                        if (string.IsNullOrEmpty(newValue) || !isDigit)
                        {
                            Console.WriteLine("Phone number value cannot be empty and must be only digits.\n");
                        }
                        else
                        {
                            person.Number = newValue;
                        }
                        break;
                    case "country":
                        if (string.IsNullOrEmpty(newValue))
                        {
                            Console.WriteLine("Country value cannot be empty.\n");
                        }
                        else
                        {
                            person.Country = newValue;
                        }
                        break;
                    case "birthday":
                        DateTime Birthday;
                        if (DateTime.TryParse(newValue, out Birthday) || string.IsNullOrEmpty(newValue))
                        {
                            person.Birthday = Birthday;
                        }
                        else
                        {
                            Console.WriteLine("The date must be entered in the following format: dd/MM/yyyy.\n");
                        }
                        break;
                    case "organization":
                        person.Organization = newValue;
                        break;
                    case "position":
                        person.Position = newValue;
                        break;
                    case "notes":
                        person.Notes = newValue;
                        break;
                    default:
                        break;
                }
            }
            else
            {
                Console.WriteLine("You can edit only the field from the proposed ones.\n");
            }
        }
    }
}
